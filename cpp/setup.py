from skbuild import setup

setup(
    name="flat_cpp",
    version="0.1.0",
    description="Example FFI package with FlatBuffers",
    author="Toshiki Teramura <toshiki.teramura@gmail.com>",
    license="MIT",
    packages=["flat_cpp"],
    install_requires=["numpy"],
)
