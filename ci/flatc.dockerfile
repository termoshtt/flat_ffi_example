FROM debian:buster as Builder
LABEL maintainer "Toshiki Teramura <toshiki.teramura@gmail.com>"

WORKDIR /
RUN apt-get update     \
 && apt-get install -y \
    git                \
    cmake              \
    g++                \
    make               \
 && apt-get clean      \
 && rm -rf /var/lib/apt/lists/*
RUN git clone https://github.com/google/flatbuffers -b 1.11.0 --depth 1
RUN cmake -Bbuild -Hflatbuffers
RUN make -C build -j flatc

FROM debian:buster-slim as Debian
COPY --from=Builder /build/flatc /usr/bin/flatc
WORKDIR /src
ENTRYPOINT ["/usr/bin/flatc"]

FROM python:3.7.3-slim as Python
COPY --from=Builder /build/flatc /usr/bin/flatc
WORKDIR /src
