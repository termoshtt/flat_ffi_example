FROM debian:buster
LABEL maintainer "Toshiki Teramura <toshiki.teramura@gmail.com>"

RUN apt-get update      \
 && apt-get install -y  \
    curl                \
    g++                 \
    make                \
    cmake               \
    ninja-build         \
    python3             \
    python3-pip         \
    python3-numpy       \
    libpython3-dev      \
 && apt-get clean       \
 && rm -rf /var/lib/apt/lists/*
RUN pip3 install --upgrade pip scikit-build
RUN curl -LO https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_70_0.tar.gz \
 && tar xf boost_1_70_0.tar.gz \
 && cd boost_1_70_0            \
 && ./bootstrap.sh --with-python=/usr/bin/python3 --with-python-version=3.7 \
 && ./b2 cxxflags=-fPIC link=static --with-python --prefix=/usr install -j 12 \
 && cd .. \
 && rm -rf boost_1_70_0*
