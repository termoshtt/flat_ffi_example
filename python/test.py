#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import flatbuffers
from Electic import FooBar


def test_foobar_construct():
    builder = flatbuffers.Builder(0)
    say = builder.CreateString("Madoka kawaii")

    FooBar.FooBarStart(builder)
    FooBar.FooBarAddHeight(builder, 150)
    FooBar.FooBarAddSay(builder, say)
    off = FooBar.FooBarEnd(builder)
    builder.Finish(off)

    buf = builder.Output()
    foobar = FooBar.FooBar.GetRootAsFooBar(buf, 0)

    assert foobar.Height() == 150
    assert foobar.Say() == b"Madoka kawaii"
